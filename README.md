# Setup

## Windows 10

1. Install [Vim for Windows](https://www.vim.org/download.php) using the
    self-installing executable.
2. Clone this repo anywhere on your machine.
3. Run `setup.bat`.
4. Cry because this is a very basic setup with no cool plugins.

## MacOS X/Debian/Mint/Arch/Manjaro

I have not actively maintained the setup for anything but WSL and Ubuntu, so
follow the Ubuntu setup instructions at your own risk.

## WSL/Ubuntu

1. Install Vim 8.2 or higher.
2. Clone this repo anywhere on your machine.
3. Run the correct installer.
    - **WSL:** `./setup.wsl.sh`
        - You may get a `permission denied` error; `chmod u+x setup.wsl.sh`
        might should clear that up
    - **Ubuntu:** `./setup.nix.sh`
4. Install and use one of the included patched fonts. The most popular dev fonts are included in the
    `fonts` directory, more can be found [here](https://github.com/ryanoasis/nerd-fonts/tree/master/patched-fonts).
    Otherwise, in `~/.vimrc`, change the `g:font_patched` variable to `0`
    and comment out the `Plugin 'ryanoasis/vim-devicons'` line.
    - Current font(s) of choice:
        - **WSL:** `DejaVu Sans Mono NF`
        - **Ubuntu:** `UbuntuMonoDerivationPowerline Nerd Font Regular`
5. Vim it up!
    - Fixes for common issues I've encountered are included below.
    - The WSL and Ubuntu setups are nearly identical, but the key additions
    to the WSL configuration are as follows:
        - Extra tweaks to handle WSL-specific weirdness
        - Some hotkeys for opening PowerShell terminals

### OmniSharp setup

#### Ubuntu

1. Install the .NET SDK: `sudo snap install dotnet-sdk --classic --channel=3.1`
2. Set up a symlink so O# can find the SDK:
    `sudo ln -s /snap/dotnet-sdk/current/dotnet /usr/local/bin/dotnet`

#### WSL

1. Download the latest version of the 64-bit stdio Windows interface from
    [here](https://github.com/OmniSharp/omnisharp-roslyn/releases).
    - If you switch to the HTTP interface, update `g:OmniSharp_server_stdio` 
2. Decompress the archive and put it in `C:\OmniSharp\` (so you should end up
    with `C:\OmniSharp\omnisharp-win-x64\OmniSharp.exe`).

##### Large, < .NET 5.0 project

Throw `omnisharp.json` in the repo's root with the following contents:
```json
{
    "msbuild": {
        "enabled": true,
        "loadProjectsOnDemand": false
    },
    "RoslynExtensionsOptions": {
        "documentAnalysisTimeoutMs": 10000, // less certain this helps
        "enableAnalyzersSupport": true,
        "threadsToUseForAnalyzers": 16 // reasonably certain this helps
    }
}
```

### Fix clipboard issues

I've had issues accessing the system keyboard using `"+`, but according to a
number of sources, installing gvim (`sudo apt-get install vim-gtk`) tends to
fix it.

#### Sources

1. https://askubuntu.com/questions/659437/y-does-not-copy-to-the-clipboard
2. https://superuser.com/questions/663353/unable-to-copy-from-vim-to-system-clipboard
3. https://askubuntu.com/questions/347519/unable-to-copy-from-vim-to-system-clipboard/434994#434994
4. https://stackoverflow.com/questions/10101488/cut-to-the-system-clipboard-from-vim-on-ubuntu
5. https://stackoverflow.com/questions/11489428/how-to-make-vim-paste-from-and-copy-to-systems-clipboard
    - Lots of answers with info about keyboards that I have not read.

### Make Vim the `git diff` and `merge` tool

```bash
git config --global diff.tool vimdiff
git config --global merge.tool vimdiff
git config --global difftool.prompt true
git config --global merge.conflictstyle diff3
git config --global alias.d difftool
```

### Fix the `ALT` key on Mac

- iTerm2: `Preferences -> Profiles -> Keys -> Left option key acts as:` set to `+Esc`
- Terminal: `Preferences -> Profiles -> Use Option as Meta key` check


# Some Key Bindings

## Comments

- Comment/Uncomment line: `gcc`
- Comment/Uncomment selection: `gc`
- Uncomment a block of commented lines: `gcgc`

## Formatting

- Fix indentation of a selected region: `=`
- Fix indentation of the whole file: `gg=G`
- Fix indentation of a current block (inside of a function/if/loop): `=aB`
- Fix indentation of a current line (in NORMAL mode): `==`

## Multiple Cursors

In order to select multiple occurrences of the same word, press:

- `Ctrl + N` to start/select next occurrence
- `Ctrl + X` to skip current selection and move to the next one
- `Ctrl + P` to select a previous word/unselect current word
- `Ctrl + S` to select all occurrences of the last search (visual: only select occurrences that are within selection)
- `,sa` to select all occurrences of the selection

After multiple occurrences are selected, any action over the selection is appropriate: `c/d/i/a` etc.
However, anything other than `c`, `d`, and `s` should be used with caution, as it has yet to work out well for me.

![Demo](https://github.com/terryma/vim-multiple-cursors/blob/master/assets/example1.gif?raw=true "Multiple Selections")

## Code structure view (just like in PyCharm)

- Show/hide the code structure: `,b`
    * Move cursor to code structure window: `,ob`

## Miscellaneous

- Toggle relative/absolute line numbers: `Alt + r`

- Highlight color code names (visual: in selection only): `,ch`
    * Clear color highlighting: `,cl`

- Go to next/previous syntax error/warning: `]e` / `[e`
    * View error detail: `,ed`

- Format the current JSON file:
    - `F8`: `JsBeautify`
    - `,jq`: `jq`

## Python/JavaScript/TypeScript/Go specific Features

This setup was originally created having **Python** and
**JavaScript/TypeScript/Go** in mind; as such, it has some advanced features:

- Full Support of **PEP8** (auto indentation, beautify)
- Syntax validation, including formatting recommendations
- Autocomplete using **Python** Jedi (IDE), including function signatures and argument hints
- IDE-level support of **TypeScript** **Go**, including function signatures and type-awareness
- JavaScript/JSON/JSX/HTML/CSS Indentation and Beautify
- Out-of-the-Box support of main **JavaScript** libraries (autocomplete, syntax, etc)
- Large collection of **Python** and **JavaScript/TypeScript/Go** snippets

### Special hotkeys

#### **JavaScript**, **TypeScript**, **Java**, **Go**, **Python**

- Fully format the whole file: `F8` (except **Java** and **Go**)
- Show symbol documentation: `,sd`
- Navigate to the location where a symbol is defined (go to definition): `,gd`
- Show location(s) where a symbol is referenced (find usages): `,fu`
- Show the type of a symbol under cursor: `,st` (except **Python**)
- Refactor (rename) a symbol under cursor: `,rn`

#### **Python**

- Run the file using **Python**: `F5`
- Run the file using **Python3**: `F6`

#### **JavaScript**

- Format selection: `Ctrl-F`
- Create a function's JSDoc: `,j` in the function body or on its signature

#### **TypeScript**

- Create a function's JSDoc: `,j` in the function body or on its signature (for
    js-style functions only)


# Neovim support (outdated)

**Not maintained, proceed with caution.**

The easiest way to use the same settings for both apps is to make symlinks for
Neovim:

```bash
ln -s ~/.vim ~/.config/nvim
ln -s ~/.vimrc ~/.config/nvim/init.vim
```

Neovim requires the python package from `pip`/`pip3` in order to function with
this setup:

```bash
pip install neovim
```

Neovim can be made to integrate with git (log pager, commit message editor,
difftool, etc.) by making the git configurations from above as well as the
following:

```bash
git config --global mergetool.vimdiff.cmd 'nvim -dR $LOCAL $REMOTE'
git config --global core.pager "nvim -R"
git config --global color.pager no
```


# How to sync with the source repository

[Instructions for adding upstream repository.](https://help.github.com/articles/configuring-a-remote-for-a-fork/)

1. Add original repository as remote upstream repository:

    `git remote add upstream git@bitbucket.org:mrpeabody/vim_setup.git`

2. Verify that the above step worked:

    `git remote -v`

    (should see `origin` and `upstream` with appropriate URLs)

[Instructions for syncing the fork](https://help.github.com/articles/syncing-a-fork/).

1. Fetch the branches from the upstream repository:

    `git fetch upstream`

2. Check out the branch you want to merge the `upstream` changes into and then merge them:

    `git checkout master`

    `git merge upstream/master`
